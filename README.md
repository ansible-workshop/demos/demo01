# Ansible Workshop
Demo 01: Playbooks

---

# Preparations

## Prepare hosts file - open host file

```
$ vim /etc/ansible/hosts
```

## Paste the following snippet in the file - nodeip will be replaced by the managed node ip(or ip's)

```
[servers]
(nodeip(s))
```

# Instructions

## Browse to port 80 in host ip (in browser) to show it is empty : 

```
http://(nodeip):80
```

## Create a new playbook

```
$ vim playbook.yml
```

## Paste the following definition

```
- hosts: servers
  become_method: sudo
  become: yes
  vars:
    http_port: 80
    max_clients: 200
  remote_user: sela
  tasks:
  - name: ensure apache is at the latest version
    apt:
      name: apache2
      state: latest
    notify:
    - restart apache
  - name: ensure apache is running
    service:
      name: apache2
      state: started
  handlers:
    - name: restart apache
      service:
        name: apache2
        state: restarted
```

## Save and quit

```
esc
:wq
```

## Run command to verify syntax in the playbook file

```
ansible-playbook playbook.yml --syntax-check
```

## Run playbook

```
ansible-playbook playbook.yml
```

## Open port 80 on host ip and show the default apache page

```
http://(nodeip):80 
```
